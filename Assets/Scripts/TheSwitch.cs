﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Analytics;

public class TheSwitch : MonoBehaviour
{
    public long CurrentTaps = 0;
    private bool StartGame = false;
    public long Switches = long.MaxValue;
    public UI_Switch SwitchUI;
    public Color OffColor;
    public Color OnColor;
    public GameObject switchOn,switchOff;
    public AudioClip switchOnSFX, switchOffSFX;
    private Camera mainCam;
    public StoreContainer store;
    public TheInstantiator instantiator;
    public bool _On = true;

    private long lastAnalyticsTaps; 
    private long sendAnalyticsThreshold = 100;
    public void SwitchOn()
    {
        
        Switches--;
        CurrentTaps++;

        if(CurrentTaps>=lastAnalyticsTaps+sendAnalyticsThreshold){
            SwitchedOnTheSwitch();
            lastAnalyticsTaps = CurrentTaps;
        }

        if (!StartGame)
        {
            GameStarter();
            StartGame = true;
        }
        //OFF
        switchOff.SetActive(false);
        //ON
        //mainCam.backgroundColor = OnColor;
        store.SwitchOnStore();
        //UPDATE THE CURRENT TAPS
        store.currentTaps.text = CurrentTaps.ToString("n0");
        instantiator.SwitchOnHelpers();
        /////////////////////////////////////
        SwitchUI.SwitchesText.color = OffColor;
        SwitchUI.SwitchesAmmount.color = OffColor;
        SwitchUI.SwitchesAmmount.text = Switches.ToString("n0");
        switchOn.GetComponent<SpriteRenderer>().color = OffColor;
        
        switchOn.SetActive(true);
        GetComponent<AudioSource>().PlayOneShot(switchOnSFX);
    }
    public void SwitchOff()
    {
        //OFF
        switchOn.SetActive(false);

        //ON
        //mainCam.backgroundColor = OffColor;
        store.SwitchOffStore();
        instantiator.SwitchOffHelpers();
        /////////////////////////////////////
        SwitchUI.SwitchesText.color = OnColor;
        SwitchUI.SwitchesAmmount.color = OnColor;
        SwitchUI.SwitchesAmmount.text = Switches.ToString("n0");
        switchOff.GetComponent<SpriteRenderer>().color = OnColor;

        switchOff.SetActive(true);
        GetComponent<AudioSource>().PlayOneShot(switchOffSFX);
    }

    private void GameStarter()
    {
        store.gameObject.GetComponentInChildren<Animation>().Play();
        SwitchUI.gameObject.GetComponentInChildren<Animation>().Play();
    }
    private void OnMouseDown()
    {
        if (switchOn.activeSelf)
        {
            //UPDATE THE CURRENT TAPS
            store.currentTaps.text = CurrentTaps.ToString("n0");
            SwitchOff();
            _On = false;
        }
        else if (switchOff.activeSelf)
        {
            //UPDATE THE CURRENT TAPS
            store.currentTaps.text = CurrentTaps.ToString("n0");
            SwitchOn();
            _On = true;
        }
    }
    private void Start()
    {
        Switches = long.MaxValue;
        mainCam = Camera.main;
        SwitchUI.SwitchesAmmount.text = Switches.ToString("n0");
    }
    private void Update()
    {
        // Code for OnMouseDown in the iPhone. Unquote to test.
        RaycastHit hit = new RaycastHit();
        for (int i = 0; i < Input.touchCount; ++i)
        {
            if (Input.GetTouch(i).phase.Equals(TouchPhase.Began))
            {
                // Construct a ray from the current touch coordinates
                Ray ray = mainCam.ScreenPointToRay(Input.GetTouch(i).position);
                if (Physics.Raycast(ray, out hit))
                {
                    hit.transform.gameObject.SendMessage("OnMouseDown");
                }
            }
        }
    }

    ///////////ANALYTICS///////////
    public void SwitchedOnTheSwitch()
    {
       AnalyticsResult analyticResult = AnalyticsEvent.Custom(
            "SwitchedOn",
            new Dictionary<string, object> {
                { "Switched", CurrentTaps }
            }
        );
        //Debug.Log("Analytics result: "+analyticResult);
    }
}
