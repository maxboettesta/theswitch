﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TheInstantiator : MonoBehaviour
{
    public int ExtraMouseCost = 50;
    public GameObject ExtraMousePrefab;
    public TextMeshProUGUI ExtraMouseCostText;

    public int TripleClickCost = 1000;
    public GameObject TripleClickPrefab;
    public TextMeshProUGUI TripleClickCostText;

    public List<GameObject> Helpers = new List<GameObject>();
    
    public void SwitchOnHelpers()
    {
        foreach (GameObject extra in Helpers)
        {
            extra.GetComponent<SpriteRenderer>().color = GetComponent<StoreContainer>().refTheSwitch.OffColor;
        }
    }
    public void SwitchOffHelpers()
    {
        foreach (GameObject extra in Helpers)
        {
            extra.GetComponent<SpriteRenderer>().color = GetComponent<StoreContainer>().refTheSwitch.OnColor;
        }
        
    }
    public void InstantiateExtraMouse()
    {
        if (GetComponent<StoreContainer>().refTheSwitch.CurrentTaps >= ExtraMouseCost)
        {
            

            GameObject newHelper = Instantiate(ExtraMousePrefab);
            if (GetComponent<StoreContainer>().refTheSwitch._On)
            {
                newHelper.GetComponent<SpriteRenderer>().color = GetComponent<StoreContainer>().refTheSwitch.OnColor;
            }
            else
            {
                newHelper.GetComponent<SpriteRenderer>().color = GetComponent<StoreContainer>().refTheSwitch.OffColor;
            }
            //SUBTRACT THE COST
            GetComponent<StoreContainer>().refTheSwitch.CurrentTaps -= ExtraMouseCost;
            ExtraMouseCost *= 2;
            ExtraMouseCostText.text = ExtraMouseCost.ToString("n0");


            Helpers.Add(newHelper);
            GetComponent<AudioSource>().Play();
        }

    }
    public void InstantiateTripleClick()
    {
        if (GetComponent<StoreContainer>().refTheSwitch.CurrentTaps >= TripleClickCost)
        {


            GameObject newHelper = Instantiate(TripleClickPrefab);
            if (GetComponent<StoreContainer>().refTheSwitch._On)
            {
                newHelper.GetComponent<SpriteRenderer>().color = GetComponent<StoreContainer>().refTheSwitch.OnColor;
            }
            else
            {
                newHelper.GetComponent<SpriteRenderer>().color = GetComponent<StoreContainer>().refTheSwitch.OffColor;
            }
            //SUBTRACT THE COST
            GetComponent<StoreContainer>().refTheSwitch.CurrentTaps -= TripleClickCost;
            TripleClickCost *= 2;
            TripleClickCostText.text = TripleClickCost.ToString("n0");

            Helpers.Add(newHelper);
            GetComponent<AudioSource>().Play();
        }

    }

    private void Start()
    {
        TripleClickCostText.text = TripleClickCost.ToString("n0");
        ExtraMouseCostText.text = ExtraMouseCost.ToString("n0");
    }
}
