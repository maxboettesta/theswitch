﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OscillateSwitch : MonoBehaviour
{
    public GameObject clickParticle;
    private TheSwitch refToSwitch;
    public float speed = 2.0f;
    public float velocityToChange = 1.0f;
    public float radiusRand = 1.5f;
    private Camera myCam;
    Vector3 mousePosinWorld;
    public HelperType myType;
    public enum HelperType
    {
        ExtraMouse,
        TripleClicker,
        Bazooka
    }
    private IEnumerator FollowMouse()
    {
        while (true)
        {
            mousePosinWorld = myCam.ScreenToWorldPoint(Input.mousePosition);
            mousePosinWorld.z = 0;
            mousePosinWorld.x = mousePosinWorld.x + Random.insideUnitCircle.x* radiusRand;
            mousePosinWorld.y = mousePosinWorld.y + Random.insideUnitCircle.y* radiusRand;
            yield return new WaitForSeconds(velocityToChange);
            ExtraMouseClick();
            GetComponent<Animation>().Play();
            yield return null;
        }
    }

    private IEnumerator FollowMouseTriple()
    {
        while (true)
        {
            mousePosinWorld = myCam.ScreenToWorldPoint(Input.mousePosition);
            mousePosinWorld.z = 0;
            mousePosinWorld.x = mousePosinWorld.x + Random.insideUnitCircle.x * radiusRand;
            mousePosinWorld.y = mousePosinWorld.y + Random.insideUnitCircle.y * radiusRand;
            yield return new WaitForSeconds(velocityToChange);
            GetComponent<Animation>().Play();
            ExtraMouseClick();
            yield return new WaitForSeconds(0.25f);
            ExtraMouseClick();
            yield return new WaitForSeconds(0.25f);
            ExtraMouseClick();
            yield return null;
        }
    }

    public void ExtraMouseClick()
    {
        //Debug.Log(gameObject.name + " Clicking");
        refToSwitch.transform.gameObject.SendMessage("OnMouseDown");
        Instantiate(clickParticle,transform.position,Quaternion.identity);
    }
    // Start is called before the first frame update
    void Start()
    {
        refToSwitch = GameObject.Find("TheSwitch").GetComponent<TheSwitch>();
        myCam = Camera.main;
        //DO DIFFERENT BEHAVIOURS DEPENDING ON THE TYPE
        switch (myType)
        {
            case HelperType.ExtraMouse:
                StartCoroutine(FollowMouse());
                //FIRST CLICK
                refToSwitch.transform.gameObject.SendMessage("OnMouseDown");
                break;
            case HelperType.TripleClicker:
                StartCoroutine(FollowMouseTriple());
                //FIRST CLICK
                refToSwitch.transform.gameObject.SendMessage("OnMouseDown");
                refToSwitch.transform.gameObject.SendMessage("OnMouseDown");
                refToSwitch.transform.gameObject.SendMessage("OnMouseDown");
                break;
            case HelperType.Bazooka:
                break;
            default:
                break;
        }
        
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Vector2.Lerp(transform.position, mousePosinWorld, speed * Time.deltaTime);
    }
}
