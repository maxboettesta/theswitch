﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroy : MonoBehaviour
{
    private float secondsTilDestroy;
    void Awake()
    {
        secondsTilDestroy = GetComponent<ParticleSystem>().main.duration;
        Destroy(gameObject,secondsTilDestroy);
    }


}
