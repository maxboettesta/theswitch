﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class StoreContainer : MonoBehaviour
{
    public TheSwitch refTheSwitch;
    public MyElements[] childrenButtons;

    public TextMeshProUGUI currentTaps;
    public void SwitchOnStore()
    {
        foreach (MyElements button in childrenButtons)
        {
            button.Background.color = refTheSwitch.OffColor;
            button.CostText.color = refTheSwitch.OffColor;
            button.Element.color = refTheSwitch.OffColor;
        }
    }
    public void SwitchOffStore()
    {
        foreach (MyElements button in childrenButtons)
        {
            button.Background.color = refTheSwitch.OnColor;
            button.CostText.color = refTheSwitch.OnColor;
            button.Element.color = refTheSwitch.OnColor;
        }
    }
    void Start()
    {
        childrenButtons = GetComponentsInChildren<MyElements>();
    }

}
