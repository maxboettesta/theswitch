﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundColor : MonoBehaviour
{
    public TheSwitch refToSwitch;
    public float lerpTime = 0.5f;
    private void Update()
    {
        if (refToSwitch._On == true)
        {
            GetComponent<Camera>().backgroundColor = Color.Lerp(GetComponent<Camera>().backgroundColor, refToSwitch.OnColor, lerpTime * Time.deltaTime);
        }
        else if(refToSwitch._On == false)
        {
            GetComponent<Camera>().backgroundColor = Color.Lerp(GetComponent<Camera>().backgroundColor, refToSwitch.OffColor, lerpTime * Time.deltaTime);
        }
       
    }
}
