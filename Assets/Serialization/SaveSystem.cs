﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem{
    
    public static void SavePlayer(TheSwitch player)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Path.Combine(Application.persistentDataPath, "theSwitchData.dat");
        FileStream stream = new FileStream(path, FileMode.Create);

        DataGame data = new DataGame(player);
        formatter.Serialize(stream, data);

        Debug.Log("Writing Data on: " + path);
        stream.Close();
    }

    public static DataGame LoadData()
    {
        string path = Path.Combine(Application.persistentDataPath, "theSwitchData.dat");
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            DataGame data = formatter.Deserialize(stream) as DataGame;
            Debug.Log("Reading Data from: " + path);
            stream.Close();
           
            return data;
        }
        else
        {
            Debug.Log("Couldn't find file: " + path);
            return null;
        }
    }
}
