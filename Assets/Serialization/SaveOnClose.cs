﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SaveOnClose : MonoBehaviour
{
    public TheSwitch mySwitch;


    private void OnApplicationPause(bool pause)
    {
        if(pause)
        SaveSystem.SavePlayer(mySwitch);
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);

        DataGame data = SaveSystem.LoadData();
        if(data!= null)
        {
            //UPDATE CURRENT TAPS
            mySwitch.CurrentTaps = data.ammountOfClicksSaved;
            //LOAD STORE COSTS
            mySwitch.instantiator.ExtraMouseCost = data.priceExtraMouse;
            mySwitch.instantiator.TripleClickCost = data.priceTrippleClick;


            //INSTANTIATE HOW MANY EXTRA CLICKERS HELPERS AS YOU HAD PREVIOUSLY
            long ammountHelpers = data.ammountOfExtraMouseHelpers;
            for (long i = 0; i < ammountHelpers; i++)
            {
                GameObject helper = Instantiate(mySwitch.instantiator.ExtraMousePrefab);
                mySwitch.instantiator.Helpers.Add(helper);
           
            }
            //INSTANTIATE HOW MANY TRIPLE CLICKERS HELPERS AS YOU HAD PREVIOUSLY
            /*
            for (long i = 0; i < ammountHelpers; i++)
            {
                GameObject helper = Instantiate(mySwitch.instantiator.TripleClickPrefab);
                mySwitch.instantiator.Helpers.Add(helper);

            }
            */
        }
    }

    private void Update()
    {
        if (Input.touchCount > 0&&Input.GetTouch(0).phase==TouchPhase.Ended)
        {
            SaveSystem.SavePlayer(mySwitch);
        }
    }
    private void OnDestroy()
    {
        SaveSystem.SavePlayer(mySwitch);
    }
}
