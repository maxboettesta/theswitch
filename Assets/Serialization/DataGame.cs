﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DataGame
{
    public long ammountOfExtraMouseHelpers;
    public long ammountOfTripleClickHelpers;
    public long ammountOfClicksSaved;
    public int priceExtraMouse;
    public int priceTrippleClick;

    public DataGame(TheSwitch player)
    {
        ammountOfClicksSaved = player.CurrentTaps;
        ammountOfExtraMouseHelpers = player.instantiator.Helpers.Count;
        ammountOfTripleClickHelpers = 0;
        priceExtraMouse = player.instantiator.ExtraMouseCost;
        priceTrippleClick = player.instantiator.TripleClickCost;
    }
}
