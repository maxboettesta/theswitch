﻿using UnityEngine;

namespace DebugStuff
{
    public class ConsoleToGUI : MonoBehaviour
    {
        //#if !UNITY_EDITOR
        static string myLog = "";
        private string output;
        private string stack;
        private GUIStyle style1 = new GUIStyle();

        void OnEnable()
        {
            style1.fontSize = 50;
            style1.wordWrap = true;
            style1.normal.textColor = Color.magenta;
            style1.fontStyle = FontStyle.Bold;
            Application.logMessageReceived += Log;
        }

        void OnDisable()
        {
            Application.logMessageReceived -= Log;
        }

        public void Log(string logString, string stackTrace, LogType type)
        {
            output = logString;
            stack = stackTrace;
            myLog = output + "\n" + myLog+"\n";
            if (myLog.Length > 50)
            {
                myLog.ToLower();
            }
        }
       
        void OnGUI()
        {
            
            
            //if (!Application.isEditor) //Do not display in editor ( or you can use the UNITY_EDITOR macro to also disable the rest)
            {
                myLog = GUI.TextArea(new Rect(10, 10, Screen.width-20, (Screen.height/4)-10), myLog, style1);
            }
        }
        //#endif
    }
}